# Heading 1
## Heading 2
### Heading 3

<!-- Horizontal Rule -->
 ___
 ---
 
| Name     | Email       |
| -------- | ----------  |
| John Doe | m@gmail.com |
| John snow| a@gmail.com |

<!-- Horizontal Rule -->
 ___
 ---


<!-- Multiline blockquote -->
>>>
This is the first line

This is the second line
>>>

| Username | Password |
| ------ | ------ |
| User 1 | pwd 1 |
| User 2 | pwd 2 |





<!-- Links -->
https://example.org/page[ A webpage]



<!-- Colors -->
HEX: `#RGB[A]`
